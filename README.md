# vue-snap-scroll-select

Small touch friendly select component for Vuejs using css scroll-snap.

## Usage

run:  
`npm i --save @nerdcel/vue-snap-scroll-select` or  
`yarn add @nerdcel/vue-snap-scroll-select`

### In your Vue main.js:

`import Vue from 'vue';`  
`import SnapScroll from '@nerdcel/vue-snap-scroll-select';`
  
`Vue.use(SnapScroll);`

### In your template
`<snap-scroll :values=[...your_values] :selected.sync="selectedIndex"></snap-scroll>`

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
