import Component from './Component.vue';

export default {
  install(Vue) {
    Vue.component('snap-select', Component);
  },
};
