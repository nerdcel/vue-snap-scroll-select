import Vue from 'vue';
import SnapSelect from './components/index';
import App from './App.vue';

Vue.use(SnapSelect);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
